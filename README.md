# Ud3_Ejemplo5
_Ejemplo 5 de la Unidad 3._

Vamos a devolver datos de una Actividad a otra creando _Intents_ explícitos. 
Para ello vamos a basarnos en el proyecto [Ud3_Ejemplo4](https://bitbucket.org/bitbucketcarlos/ud3_ejemplo4) y realizaremos los siguientes cambios (los demás ficheros no necesitarán cambios salvo el nombre del proyecto):

## _actividad1.xml_ y _actividad2.xml_

En este ejemplo vamos a añadir un botón que será el que devuelva los datos a la actividad principal.
Es necesario que tanto los botones como los _TextView_ tengan sus _ids_ para posteriormente buscarlos.

_actividad1.xml_:

```html
...
    <Button
        android:id="@+id/botonAct1"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginTop="24dp"
        android:layout_marginEnd="8dp"
        android:layout_marginRight="8dp"
        android:text="@string/boton"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/TextViewAct1" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

_actividad2.xml_:

```html
...
    <Button
        android:id="@+id/botonAct2"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginTop="24dp"
        android:layout_marginEnd="8dp"
        android:layout_marginRight="8dp"
        android:text="@string/boton"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/TextViewAct2" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

## _MainActivity.kt_
En el fichero _MainActivity.kt_ deberemos hacer varios cambios.

Primero tendremos que registrar la respuesta de los _intents_ enviados. Para ello hacemos uso de la clase _ActivityResultLauncher_ y del método
_registerForActivityResult_. Usaremos, además, la clase _ActivityResultContracts_ que nos permite utilizar una serie de clases útiles como capturar vídeo o 
abrir documentos. 

Finalmente com el método _onActivityResult_ del _callback_ _ActivityResultCallback_ nos devuelve la respuesta al _intent_. En él comprobaremos el código de petición
 y si el código de resultado es correcto. Si es así usaremos la clase _Toast_ para mostrar el mensaje por pantalla con el valor devuelto. Para ello haremos uso 
 de una función Lambda donde en la variable _resul_ tendremos el resultado de la llamada realizada, a partir de ella podemos obtener tanto el código enviado como los datos.
Observad que en el primer caso se ha devuelto un _String_ y en el segundo un entero:

```java
...
    val resultAct1 = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val datosResult = result.data

            Toast.makeText(this, datosResult?.getStringExtra(DATO_DEVUELTO), Toast.LENGTH_LONG).show()
        }
    }

    val resultAct2 = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val datosResult = result.data

            Toast.makeText(this, datosResult?.getIntExtra(DATO_DEVUELTO, VALOR_DEFECTO).toString(), Toast.LENGTH_LONG).show()
        }
    }
...
```

A continuación, en el método _onCreate_ creamos y lanzamos los _intents_ con el método _launch_ de las variables de tipo _ActivityResultLauncher_ creadas anteriormente:
```java
...
        binding.actividad1.setOnClickListener {
            val intentAct1 = Intent(this, Actividad1::class.java).apply {
                putExtra(Intent.EXTRA_TEXT,"Texto Actividad 1")
            }

            resultAct1.launch(intentAct1)
        }

        binding.actividad2.setOnClickListener {
            val intentAct2 = Intent(this, Actividad2::class.java).apply {
                putExtra(Intent.EXTRA_TEXT,"Texto Actividad 2")
            }

            resultAct2.launch(intentAct2)
        }
...
```

## _Actividad1.kt_ y _Actividad2.kt_

El último cambio que debemos hacer es asignar un _Click Listener_ a los botones y devolver el resultado creando un _Intent_, 
rellenando los datos extra con el valor que se quiere devolver junto con el código _RESULT_OK_ para indicar que todo ha ido bien.
Por último llamamos al método _finish_ para cerrar la actividad y volver a atrás.

_Actividad1.kt_:
```java
...
        // Comprobamos si el intent tiene datos extra
        if(intent.hasExtra(Intent.EXTRA_TEXT))
            binding.TextViewAct1.text = intent.getStringExtra(Intent.EXTRA_TEXT)


        // Asignamos un Click Listener al botón para devolver los datos a la actividad principal
        binding.botonAct1.setOnClickListener {
            val intent = Intent().apply {
                // Devolvemos un String
                putExtra(DATO_DEVUELTO,"String devuelto")
            }

            // Devolvemos un código de RESULT_OK
            setResult(RESULT_OK, intent)

            // Cerramos la actividad y volvemos a atrás.
            finish()
        }
    }
}
```

_Actividad2.kt_:
```java
...
        // Comprobamos si el intent tiene datos extra
        if(intent.hasExtra(Intent.EXTRA_TEXT))
            binding.TextViewAct2.text = intent.getStringExtra(Intent.EXTRA_TEXT)


        // Asignamos un Click Listener al botón para devolver los datos a la actividad principal
        binding.botonAct2.setOnClickListener {
            val intent = Intent().apply {
                // Devolvemos un String
                putExtra(MainActivity.DATO_DEVUELTO,101)
            }

            // Devolvemos un código de RESULT_OK
            setResult(RESULT_OK, intent)

            // Cerramos la actividad y volvemos a atrás.
            finish()
        }
    }
}
```
