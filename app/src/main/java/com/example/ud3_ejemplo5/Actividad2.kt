package com.example.ud3_ejemplo5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ud3_ejemplo5.databinding.Actividad1Binding
import com.example.ud3_ejemplo5.databinding.Actividad2Binding

class Actividad2 : AppCompatActivity() {

    private lateinit var binding: Actividad2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = Actividad2Binding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Comprobamos si el intent tiene datos extra
        if(intent.hasExtra(Intent.EXTRA_TEXT))
            binding.TextViewAct2.text = intent.getStringExtra(Intent.EXTRA_TEXT)


        // Asignamos un Click Listener al botón para devolver los datos a la actividad principal
        binding.botonAct2.setOnClickListener {
            val intent = Intent().apply {
                // Devolvemos un String
                putExtra(MainActivity.DATO_DEVUELTO,101)
            }

            // Devolvemos un código de RESULT_OK
            setResult(RESULT_OK, intent)

            // Cerramos la actividad y volvemos a atrás.
            finish()
        }
    }
}