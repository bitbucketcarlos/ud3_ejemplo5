package com.example.ud3_ejemplo5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ud3_ejemplo5.MainActivity.Companion.DATO_DEVUELTO
import com.example.ud3_ejemplo5.databinding.Actividad1Binding

class Actividad1 : AppCompatActivity() {

    private lateinit var binding: Actividad1Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = Actividad1Binding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Comprobamos si el intent tiene datos extra
        if(intent.hasExtra(Intent.EXTRA_TEXT))
            binding.TextViewAct1.text = intent.getStringExtra(Intent.EXTRA_TEXT)


        // Asignamos un Click Listener al botón para devolver los datos a la actividad principal
        binding.botonAct1.setOnClickListener {
            val intent = Intent().apply {
                // Devolvemos un String
                putExtra(DATO_DEVUELTO,"String devuelto")
            }

            // Devolvemos un código de RESULT_OK
            setResult(RESULT_OK, intent)

            // Cerramos la actividad y volvemos a atrás.
            finish()
        }
    }
}