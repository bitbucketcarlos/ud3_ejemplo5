package com.example.ud3_ejemplo5

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import com.example.ud3_ejemplo5.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    // Todas las variables del bloque 'companion object' son estáticas
    companion object {
        // Los datos devueltos tendrán como nombre asociado el valor de la constante.
        const val DATO_DEVUELTO:String = "DATO_DEVUELTO"

        // Constante para el valor por defecto del método getIntExtra del Intent.
        const val VALOR_DEFECTO:Int = 0
    }

    /* Preparamos la respuesta de los Intents, para ello registramos la respuesta de la llamada haciendo uso
       del método registerForActivityResult.
       La clase ActivityResultContracts nos permite utilizar una serie de clases útiles como capturar vídeo o
       abrir documentos.
       El método onActivityResult del callback ActivityResultCallback nos devuelve la respuesta al Intent. Para ello hacemos uso
       de una función Lambda donde en la variable 'result' tenemos el resultado de la llamada realizada, a partir de ella podemos
       obtener tanto el código enviado como los datos.
    */
    val resultAct1 = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val datosResult = result.data

            Toast.makeText(this, datosResult?.getStringExtra(DATO_DEVUELTO), Toast.LENGTH_LONG).show()
        }
    }

    val resultAct2 = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val datosResult = result.data

            Toast.makeText(this, datosResult?.getIntExtra(DATO_DEVUELTO, VALOR_DEFECTO).toString(), Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        binding.actividad1.setOnClickListener {
            val intentAct1 = Intent(this, Actividad1::class.java).apply {
                putExtra(Intent.EXTRA_TEXT,"Texto Actividad 1")
            }

            resultAct1.launch(intentAct1)
        }

        binding.actividad2.setOnClickListener {
            val intentAct2 = Intent(this, Actividad2::class.java).apply {
                putExtra(Intent.EXTRA_TEXT,"Texto Actividad 2")
            }

            resultAct2.launch(intentAct2)
        }

    }
}